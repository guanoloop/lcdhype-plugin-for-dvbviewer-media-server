{

LCDHype Plugin for DVBViewer Media Server 2.0.4.0

Plugin written by B.Lenherr V0.7 - 15.03.2018

}

unit DVBViewerMediaServerEx;


interface


uses
  Vcl.Dialogs,
  System.SysUtils,
  System.Classes,
  ScriptEngineInterface,
  IdHTTP,
  xmldom,
  XMLIntf,
  XMLDoc,
  StrUtils;


const //ID sets the description of the dll
  ID   = 'DVBViewerMS';

  //if an array elem shows this value it marks the end of data
  EOL  = #0;
  MAX_AREAS = 255;



  coLCDHypeBooleanValue: array[Boolean] of UnicodeString = ('False', 'True');


//TData struct required for plugin
type TData  = record
  data : array[0..65535] of byte;
end;

//pixel informations of gfx output
TVRAMBitmap = array[0..319,0..319] of byte;

//list of refresh areas set by plugin for correct drawing of gfx output
TAreas      = array[0..MAX_AREAS,0..3] of integer;

//configuration information data from lcdhype
TConfigInfo = record
  gLCDx : word; //width of gLCD in pixels
  gLCDy : word; //height of gLCD in pixels
end;


procedure ConnectToDVBViewerMSTimerlist(const CHost, CPort, CPath, CUser, CPass : UnicodeString);
procedure ConnectToDVBViewerMSRecordings(const CHost, CPort, CPath, CUser, CPass : UnicodeString);


var
  TimerlistDate        : TStringList;
  TimerlistStartTime   : TStringList;
  TimerlistDuration    : TStringList;
  TimerlistEndTime     : TStringList;
  TimerlistDescription : TStringList;
  TimerlistChannelName : TStringList;
  TimerlistExecuteable : TStringList;
  TimerlistRecording   : TStringList;

  RecordingsId          : TStringList;
  RecordingsDate        : TStringList;
  RecordingsStartTime   : TStringList;
  RecordingsDuration    : TStringList;
  RecordingsChannelName : TStringList;
  RecordingsTitle       : TStringList;
  RecordingsFile        : TStringList;
  RecordingsDescription : TStringList;

  TimerlistConnection      : TScriptFunctionImplementation;
  TimerlistCountTimer      : TScriptFunctionImplementation;
  TimerlistGetRecordStatus : TScriptFunctionImplementation;
  TimerlistGetData         : TScriptFunctionImplementation;

  RecordingsConnection      : TScriptFunctionImplementation;
  RecordingsCountRecordings : TScriptFunctionImplementation;
  RecordingsGetData         : TScriptFunctionImplementation;

implementation


{$REGION ' Plugin Helper '}


procedure ConnectToDVBViewerMSTimerlist(const CHost, CPort, CPath, CUser, CPass : UnicodeString);
var
  IdHTTP: TIdHTTP;
  xmlDoc : IXMLDocument;
  xmlStrem: TMemoryStream;
  i: LongInt;
begin
  xmlDoc := nil;
  TimerlistDate        := TStringList.Create;
  TimerlistStartTime   := TStringList.Create;
  TimerlistDuration    := TStringList.Create;
  TimerlistEndTime     := TStringList.Create;
  TimerlistDescription := TStringList.Create;
  TimerlistChannelName := TStringList.Create;
  TimerlistExecuteable := TStringList.Create;
  TimerlistRecording   := TStringList.Create;

  IdHTTP := TIdHTTP.Create;
  IdHTTP.HandleRedirects := True;
  IdHTTP.ConnectTimeout := 60000;
  IdHTTP.Request.Clear;
  IdHTTP.Request.BasicAuthentication := true;
  IdHTTP.Request.Username := CUser;
  IdHTTP.Request.Password := CPass;
  xmlStrem := TMemoryStream.Create;
  IdHTTP.Get('http://' + CHost + ':' + CPort + CPath, xmlStrem);
  xmlDoc := newXMLDocument;
  xmlDoc.LoadFromStream(xmlStrem);
  xmldoc.Active := true;
  for i := 0 to xmlDoc.ChildNodes['Timers'].ChildNodes.Count - 1 do
  begin
    TimerlistDate.Add(xmlDoc.DocumentElement.ChildNodes[i].Attributes['Date']);
    TimerlistStartTime.Add(LeftStr(xmlDoc.DocumentElement.ChildNodes[i].Attributes['Start'],5));
    TimerlistDuration.Add(xmlDoc.DocumentElement.ChildNodes[i].Attributes['Dur']);
    TimerlistEndTime.Add(LeftStr(xmlDoc.DocumentElement.ChildNodes[i].Attributes['End'],5));
    TimerlistDescription.Add(xmlDoc.DocumentElement.ChildNodes[i].ChildNodes['Descr'].Text);
    TimerlistChannelName.Add(Copy(xmlDoc.DocumentElement.ChildNodes[i].ChildNodes['Channel'].Attributes['ID'], 21, 255));
    TimerlistExecuteable.Add(IntToStr(abs(StrToInt(xmlDoc.DocumentElement.ChildNodes[i].ChildNodes['Executeable'].Text))));
    TimerlistRecording.Add(IntToStr(abs(StrToInt(xmlDoc.DocumentElement.ChildNodes[i].ChildNodes['Recording'].Text))));
  end;
  if Assigned(IdHTTP) then IdHTTP.Destroy;
  if Assigned(xmlStrem) then xmlStrem.Destroy;
end;


procedure ConnectToDVBViewerMSRecordings(const CHost, CPort, CPath, CUser, CPass : UnicodeString);
var
  IdHTTP: TIdHTTP;
  xmlDoc : IXMLDocument;
  xmlStrem: TMemoryStream;
  i: LongInt;
  CDay, CMonth, CYear, CHour, CMinute: string;
begin
  xmlDoc := nil;
  RecordingsId          := TStringList.Create;
  RecordingsDate        := TStringList.Create;
  RecordingsStartTime   := TStringList.Create;
  RecordingsDuration    := TStringList.Create;
  RecordingsChannelName := TStringList.Create;
  RecordingsFile        := TStringList.Create;
  RecordingsTitle       := TStringList.Create;
  RecordingsDescription := TStringList.Create;

  IdHTTP := TIdHTTP.Create;
  IdHTTP.HandleRedirects := True;
  IdHTTP.ConnectTimeout := 60000;
  IdHTTP.Request.Clear;
  IdHTTP.Request.BasicAuthentication := true;
  IdHTTP.Request.Username := CUser;
  IdHTTP.Request.Password := CPass;
  xmlStrem := TMemoryStream.Create;
  IdHTTP.Get('http://' + CHost + ':' + CPort + CPath, xmlStrem);
  xmlDoc := newXMLDocument;
  xmlDoc.LoadFromStream(xmlStrem);
  xmldoc.Active := true;
  for i := 2 to xmlDoc.ChildNodes['recordings'].ChildNodes.Count - 1 do
  begin
    RecordingsId.Add(xmlDoc.DocumentElement.ChildNodes[i].Attributes['id']);
    CYear := copy(xmlDoc.DocumentElement.ChildNodes[i].Attributes['start'], 0, 4);
    CMonth := copy(xmlDoc.DocumentElement.ChildNodes[i].Attributes['start'], 5, 2);
    CDay := copy(xmlDoc.DocumentElement.ChildNodes[i].Attributes['start'], 7, 2);
    CHour := copy(xmlDoc.DocumentElement.ChildNodes[i].Attributes['start'], 9, 2);
    CMinute := copy(xmlDoc.DocumentElement.ChildNodes[i].Attributes['start'], 11, 2);
    RecordingsDate.Add(CDay + '.' + CMonth + '.' + CYear);
    RecordingsStartTime.Add(CHour + ':' + CMinute);
    CHour := copy(xmlDoc.DocumentElement.ChildNodes[i].Attributes['duration'], 0, 2);
    CMinute := copy(xmlDoc.DocumentElement.ChildNodes[i].Attributes['duration'], 3, 2);
    RecordingsDuration.Add((inttostr((strtoint(CHour) * 60) + strtoint(CMinute))));
    RecordingsChannelName.Add(xmlDoc.DocumentElement.ChildNodes[i].ChildNodes['channel'].Text);
    RecordingsFile.Add(xmlDoc.DocumentElement.ChildNodes[i].ChildNodes['file'].Text);
    RecordingsTitle.Add(xmlDoc.DocumentElement.ChildNodes[i].ChildNodes['title'].Text);
    RecordingsDescription.Add(xmlDoc.DocumentElement.ChildNodes[i].ChildNodes['desc'].Text);
  end;

  if Assigned(IdHTTP) then IdHTTP.Destroy;
  if Assigned(xmlStrem) then xmlStrem.Destroy;
  FreeAndNil(CDay);
  FreeAndNil(CMonth);
  FreeAndNil(CYear);
  FreeAndNil(CHour);
  FreeAndNil(CMinute);
end;


function TimerlistGetDataEx(const GData, GId : UnicodeString): PWideChar; stdcall;
var
  GReturn: WideString;
begin
  GReturn := '';
  if GData = 'Date' then
  begin
    GReturn := TimerlistDate[StrToInt(GId)];
  end
  else if GData = 'StartTime' then
  begin
    GReturn := TimerlistStartTime[StrToInt(GId)];
  end else if GData = 'Duration' then
  begin
    GReturn := TimerlistDuration[StrToInt(GId)];
  end
  else if GData = 'EndTime' then
  begin
    GReturn := TimerlistEndTime[StrToInt(GId)];
  end
  else if GData = 'Description' then
  begin
    GReturn := TimerlistDescription[StrToInt(GId)];
  end
  else if GData = 'ChannelName' then
  begin
    GReturn := TimerlistChannelName[StrToInt(GId)];
  end
  else if GData = 'Executeable' then
  begin
    GReturn := TimerlistExecuteable[StrToInt(GId)];
  end
  else if GData = 'Recording' then
  begin
    GReturn := TimerlistRecording[StrToInt(GId)];
  end;
  result := PWideChar(GReturn);
end;


function RecordingsGetDataEx(const GData, GId : UnicodeString): PWideChar; stdcall;
var
  GReturn: WideString;
begin
  GReturn := '';
  if GData = 'Id' then
  begin
    GReturn := RecordingsId[StrToInt(GId)];
  end
  else if GData = 'Date' then
  begin
    GReturn := RecordingsDate[StrToInt(GId)];
  end
  else if GData = 'StartTime' then
  begin
    GReturn := RecordingsStartTime[StrToInt(GId)];
  end
  else if GData = 'Duration' then
  begin
    GReturn := RecordingsDuration[StrToInt(GId)];
  end
  else if GData = 'ChannelName' then
  begin
    GReturn := RecordingsChannelName[StrToInt(GId)];
  end
  else if GData = 'File' then
  begin
    GReturn := RecordingsFile[StrToInt(GId)];
  end
  else if GData = 'Title' then
  begin
    GReturn := RecordingsTitle[StrToInt(GId)];
  end
  else if GData = 'Description' then
  begin
    GReturn := RecordingsDescription[StrToInt(GId)];
  end;
  result := PWideChar(GReturn);
end;


{$ENDREGION}




{$REGION ' Plugin interface '}


{$ENDREGION}




{$REGION ' Script engine interface '}

// Plugin.DVBViewerMS.Timerlist.Connection
function Library_TimerlistConnection(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lHost, lPort, lPath, lUser, lPass : PWideChar;
  lScriptFunctionParameter : PScriptFunctionParameter;
begin
  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then
  begin
    if AParameter^.ParameterCount = 5 then
    begin
      lScriptFunctionParameter := AParameter.ParameterList;
      lHost := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lPort := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lPath := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lUser := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lPass := lScriptFunctionParameter^.Value;
      ConnectToDVBViewerMSTimerlist(lHost, lPort, lPath, lUser, lPass);
    end;
  end;
  result := '';
end;


// Plugin.DVBViewerMS.Timerlist.CountTimer
function Library_TimerlistCountTimer(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
begin
  result := PWideChar(#39 + IntToStr(TimerlistRecording.Count) + #39);
end;


// Plugin.DVBViewerMS.Timerlist.GetRecordStatus
function Library_TimerlistGetRecordStatus(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  tr : LongInt;
  i : LongInt;
begin
  tr := 0;
  for i := 0 to TimerlistRecording.Count - 1 do
  begin
    if TimerlistRecording[i] = '1' then
      tr := tr + 1;
  end;
  result := PWideChar(#39 + IntToStr(tr) + #39);
end;


// Plugin.DVBViewerMS.Timerlist.GetData
function Library_TimerlistGetData(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lData, lId : PWideChar;
  gReturnValue: WideString;
  lScriptFunctionParameter : PScriptFunctionParameter;
begin
  gReturnValue := '';
  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then
  begin
    if AParameter^.ParameterCount = 2 then
    begin
      lScriptFunctionParameter := AParameter.ParameterList;
      lData := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lId := lScriptFunctionParameter^.Value;
      gReturnValue := TimerlistGetDataEx(lData, lId);
    end;
  end;
  result := PWideChar(#39 + gReturnValue + #39);
end;


// Plugin.DVBViewerMS.Recordings.Connection
procedure Library_RecordingsConnection(const AParameter: PScriptFunctionImplementationParameter);
var
  lHost, lPort, lPath, lUser, lPass : PWideChar;
  lScriptFunctionParameter : PScriptFunctionParameter;
begin
  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then
  begin
    if AParameter^.ParameterCount = 5 then
    begin
      lScriptFunctionParameter := AParameter.ParameterList;
      lHost := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lPort := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lPath := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lUser := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lPass := lScriptFunctionParameter^.Value;
      ConnectToDVBViewerMSRecordings(lHost, lPort, lPath, lUser, lPass);
    end;
  end;
end;


// Plugin.DVBViewerMS.Recordings.CountRecordings
function Library_RecordingsCountRecordings(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
begin
  result := PWideChar(#39 + IntToStr(RecordingsDate.Count) + #39);
end;


// Plugin.DVBViewerMS.Recordings.GetData
function Library_RecordingsGetData(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lData, lId : PWideChar;
  gReturnValue: WideString;
  lScriptFunctionParameter : PScriptFunctionParameter;
begin
  gReturnValue := '';
  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then
  begin
    if AParameter^.ParameterCount = 2 then
    begin
      lScriptFunctionParameter := AParameter.ParameterList;
      lData := lScriptFunctionParameter^.Value;
      Inc(lScriptFunctionParameter);
      lId := lScriptFunctionParameter^.Value;
      gReturnValue := RecordingsGetDataEx(lData, lId);
    end;
  end;
  result := PWideChar(#39 + gReturnValue + #39);
end;


{$ENDREGION}


initialization
  TimerlistConnection      := @Library_TimerlistConnection;
  TimerlistCountTimer      := @Library_TimerlistCountTimer;
  TimerlistGetRecordStatus := @Library_TimerlistGetRecordStatus;
  TimerlistGetData         := @Library_TimerlistGetData;

  RecordingsConnection      := @Library_RecordingsConnection;
  RecordingsCountRecordings := @Library_RecordingsCountRecordings;
  RecordingsGetData         := @Library_RecordingsGetData;
end.

