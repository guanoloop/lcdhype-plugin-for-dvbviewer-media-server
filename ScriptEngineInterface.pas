//
// LCDHype script engine interface, requires LCDHype version 0.7.4.5 or above
//
// $Id: ScriptEngineInterface.pas 7 2011-05-14 15:17:50Z LostSoul $
//

unit ScriptEngineInterface;

{$Z4}
{$A4} // we use 4 byte alignment because packed record costs too much performance

interface

const
  coScriptEngineInterfaceRevision = '$Revision: 7 $';

type
  PScriptFunctionParameter = ^TScriptFunctionParameter;
  TScriptFunctionParameter = record
    StructureSize: LongWord;
    Name: PWideChar;
    Value: PWideChar;
  end;

  PScriptFunctionImplementationParameter = ^TScriptFunctionImplementationParameter;
  TScriptFunctionImplementationParameter = record
    StructureSize: LongWord;
    Name: PWideChar;  // script function name, this is the string between % and (
    ParameterCount: LongWord; // number of TScriptFunctionParameter structures passed by ParameterList
    ParameterList: PScriptFunctionParameter;
  end;

  // returned PChar needs to be a valid pointer (e.g. from a global return value
  // buffer) to the first character of the return value, due to performance
  // reasons lcdhype does not free the memory behind the returned pointer,
  // so be cautious with the returned pointer to avoid memory leaks
  TScriptFunctionImplementation = function (const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;

implementation

end.
