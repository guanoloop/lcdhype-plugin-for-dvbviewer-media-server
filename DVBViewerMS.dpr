{

LCDHype Plugin for DVBViewer Media Server 2.0.4.0

Plugin written by B.Lenherr V0.7 - 15.03.2018

}

library DVBViewerMS;


uses
  ScriptEngineInterface in 'ScriptEngineInterface.pas',
  DVBViewerMediaServerEx in 'DVBViewerMediaServerEx.pas';

{$R *.RES}


exports TimerlistConnection;
exports TimerlistCountTimer;
exports TimerlistGetRecordStatus;
exports TimerlistGetData;

exports RecordingsConnection;
exports RecordingsCountRecordings;
exports RecordingsGetData;

end.
